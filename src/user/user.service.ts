import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entity/user.entity';
import { jwtConstants } from './constants';
import * as jwt from 'jsonwebtoken';
import { UpdateResult, DeleteResult } from  'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return await this.usersRepository.find();
  }

  async create(user: User): Promise<User> {
    return await this.usersRepository.save(user);
  }

  async update(id: number, user: User): Promise<UpdateResult> {
    return await this.usersRepository.update(id, user);
  }

  async findOne(id: any): Promise<User> {
    return await this.usersRepository.findOne(id);
  }

  async remove(id: any): Promise<DeleteResult> {
    return await this.usersRepository.delete(id);
  }

  createToken(user) {
    const expiresIn = 3600;
    const accessToken = jwt.sign({
        username: user.email,
        name: user.name }, jwtConstants.secret, { expiresIn });
    return {
        expiresIn,
        accessToken,
    };
  }
}