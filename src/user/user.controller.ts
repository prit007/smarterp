import { Controller, Get, Res, Body, Post, NotFoundException, HttpStatus, Query, UseGuards, Request, Delete, Put, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateUserDTO, CreateLoginDTO } from './dto/user.dto';
import * as bcrypt from 'bcryptjs';
import { User, LoginUser } from './entity/user.entity';
import { DeleteResult, UpdateResult } from 'typeorm';

@Controller('user')
export class UserController {    
    
    constructor(private readonly userService: UserService) {}  
    
    @Post('/signup')    
    async addCustomer(@Res() res, @Body() userData: any) {
        const user: User = await this.userService.findOne({email: userData.email});
        if (!user) {
            (userData.password as any) = await bcrypt.hash(userData.password, 10);
            const lists = await this.userService.create(userData);            
            const token = this.userService.createToken(lists);
            return res.status(HttpStatus.OK).json({
                message: "User has been created successfully",
                success: true,
                token: token,
                data: lists
            })
          } else {
            return res.status(HttpStatus.OK).json({
                message: "User already exist",
                success: false              
            })   
        }     
        
    }
    
    @Post('login')
    public async login(@Res() res, @Body() userData: LoginUser) {
        let isPasswordValid: boolean;
        const user: User = await this.userService.findOne({email: userData.email});
        if (user) {
            try {
                isPasswordValid = await bcrypt.compare(userData.password, user.password);
            }
            catch (error) {
                throw new InternalServerErrorException(`An error occured during password comparison  ${error.toString()}`);
            }
            if (isPasswordValid === false) {
                throw new UnauthorizedException(`Invalid password for user (${user.name})`);
            }
            const token = this.userService.createToken(user);
            return res.status(HttpStatus.OK).json({
                message: "User has been loggedin successfully",
                success: true,
                token: token,
                data: user
            })
            
            res.status(HttpStatus.OK).json(token);
        } else {
            throw new UnauthorizedException(`User does not exist`);
        }
    }

    @Get('all')
    async findAll(@Res() res) {
        const lists = await this.userService.findAll();
        return res.status(HttpStatus.OK).json(lists);
    }

    @Get()
    async findByEmail(@Res() res, @Query('email') id: string) {
        const lists = await await this.userService.findOne({email: id});
        if (!lists) throw new NotFoundException('Email does not exist!');
        return res.status(HttpStatus.OK).json(lists);
    }

    // @Get('expertprofile')
    // async getExpertProfile(@Res() res) {
    //     const lists = await this.userService.findOne(this.id);
    //     if (!lists) throw new NotFoundException('Id does not exist!');
    //     return res.status(HttpStatus.OK).json(lists);
    // }

    @Put('/update')
    async update(@Res() res, @Body() userData: User) {        
        const lists: User = await this.userService.findOne({email: userData.email});
        if (!lists) {
            throw new NotFoundException('Post does not exist');
        } else {
            const updateList: UpdateResult = await this.userService.update(lists.id, userData);
            return res.status(HttpStatus.OK).json({
                message: 'Post has been successfully updated',
                updateList
            })
        }        
    }

    @Delete('/delete')
    async delete(@Res() res, @Body() userData: User) {
        const lists: User = await this.userService.findOne({email: userData.email});
        if (!lists) {
            throw new NotFoundException('Post does not exist');
        } else {
            const deleteList: DeleteResult = await this.userService.remove(lists.id);
            return res.status(HttpStatus.OK).json({
                message: 'Post has been deleted',
                deleteList
            })
        }
    }

}
