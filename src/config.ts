export const config: any = {
    prodMongoUrl: "mongodb://covid:covid19@ds151066.mlab.com:51066/heroku_8xshcg29",
    qaMongoUrl: "mongodb://admin:Password1@ds061711.mlab.com:61711/heroku_7q1xt2kd",
    masterlistCollectionPath: "masterlist",
    totallistDocumentPath: "totallist",
    userPredefinedEmails: ["super@pvmagachogmail.onmicrosoft.com", "jdoe@pvmagachogmail.onmicrosoft.com", "mjane@pvmagachogmail.onmicrosoft.com"],
    userRoles: ['Technical', 'Expert'],
    swaggerApiTitle: 'Smart ERP API',
    swaggerApiDescription: "Smart ERP Backend API created by using Nest JS."
}

export enum UserRole {
    Technical = 'Technical',
    Expert = 'Expert',
}