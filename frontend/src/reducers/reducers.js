//Combine all Reducers of Redux
import { combineReducers } from 'redux';
// import {homeReducer} from './home/homeReducer';
// import {screeningReducer} from './screening/screeningReducer';
// import {applyReducer} from './apply/applyReducer';
// import {providerReducer} from './provider/providerReducer';
// import { consumerReducer } from './consumer/consumerReducer';
// import { routerReducer } from 'react-router-redux';
// import { languageReducer } from './common/language/languageReducer';
import { reducer as formReducer } from 'redux-form'
import { alert } from './alert.reducer';
import { authentication } from './authentication.reducer';
import { users } from './users.reducer';

 const rootReducer = combineReducers({
    form: formReducer,
    alert: alert,
    authentication, authentication,
    users, users
    // home : homeReducer,
    // apply: applyReducer,
    // provider: providerReducer,
    // consumer: consumerReducer,
    // router: routerReducer,
    // language :languageReducer,
    // screening: screeningReducer   
});

export default rootReducer;

export const mapStateToProps = (state) => {
    return {
        // home: state.home,
        // language : state.language.constantStrings,
        // selectedLanguage : state.language.selectedLanguage,
        // screening: state.screening,
        // provider: state.provider,
        // apply: state.apply,
        // consumer: state.consumer
    }
}