
import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Router} from 'react-router-dom';
import NotFound from './components/common/404';
import HomePage from './components/homePage/homePage';
import Login from './components/login/login';
import { PrivateRoute } from './helpers/PrivateRoute';
import Dashboard from './components/dashboard/dashboard';

class Routes extends Component {
  render() {
    return (
    <BrowserRouter>
    <Router history={this.props.history}>
      <Switch>       
        <PrivateRoute exact path="/" component={Dashboard} />    
        <Route  path="/home" component={HomePage} />
        <Route component={NotFound}/>
      </Switch>
    </Router>
    </BrowserRouter>     
    );
  }
}

export default Routes;
