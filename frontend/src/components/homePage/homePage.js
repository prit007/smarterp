import React, {Component} from 'react';
import './HomePage.css';
import Login from '../login/login';
import Tabs from "../common/Tabs";
import Register from '../register/register';
import { userActions } from '../../actions/user.action';
import { connect } from 'react-redux';

class HomePage extends Component {

  constructor(props) {
    super(props);
    // reset login status
    this.props.dispatch(userActions.logout());
  }


  render() {
    return (
      <React.Fragment>
        <div className="home-page">
          <div className="left-content">
            
          </div>
          <div className="right-content">
          <Tabs>
          <div label="Register">
      <Register />
      </div>   
      <div label="Login">
        <Login />
      </div>
       
    </Tabs>
            
          </div>
        </div>
      </React.Fragment>

    );
  }
}

export default connect()(HomePage);
