//Global Routes
import React, { Component } from 'react';
import './register.css';
import RegisterForm from './registerForm';
import { userActions } from '../../actions/user.action';
import { connect } from 'react-redux';

class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
        name: '',
        email: '',
        password: '',
        submitted: false
    };
    this.submit = this.submit.bind(this);
  }

  async submit (values) {
    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
    await sleep(500);
    console.log(values);
    const { dispatch } = this.props;
    dispatch(userActions.register(values));
  }


  render() {
    return (

   <div className="register-page">    
       <h1 className="login-header">Register</h1>
       <RegisterForm onSubmit={this.submit} />       
   </div>


    );
  }
}

function mapStateToProps(state) {
  const { loggingIn } = state.authentication;
  return {
      loggingIn
  };
}

export default connect(mapStateToProps)(Register);
