import React from 'react'
import { Field, reduxForm } from 'redux-form';
import './registerForm.css'
import { required, email } from '../../constants/ValidationConstant';
import asyncValidate  from './asyncValidateRegister';
import { connect } from 'react-redux';
import validate from './validate';

const renderField = ({ input, label, type, meta: { asyncValidating, touched, error, warning } }) => (
    <div  className={asyncValidating ? 'async-validating form-field-col' : 'form-field-col'}>
      {/* <label>{label}</label> */}
      
        <input className={touched ? error ? "errorField": "successField": null } {...input} placeholder={label} type={type}/>
        {touched && ((error && <span className="error">{error}</span>) || (warning && <span>{warning}</span>))}
      
    </div>
)
  

let RegisterForm = props => {
  const { handleSubmit, pristine, reset, submitting, loggingIn } = props
  return (
    <form onSubmit={handleSubmit} className="register-form">
      <div className="form-field-row-inline">
            <Field name="name" component={renderField} label="Name" type="text" validate={[required]} />
            <Field name="email" component={renderField} label="Email" type="email" validate={[required, email]} />
            <Field name="password" component={renderField} label="Password" type="password" validate={[ required ]} />
      </div>
      
        <div className="submit-block">
            <button className="primary-button" type="submit" disabled={submitting}>Submit</button>
            {loggingIn &&
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
            <button className="default login-reset-button" type="button" disabled={pristine || submitting} onClick={reset}>Reset</button>
            {/* <button type="submit" className="primary-button" type="submit">Submit</button> */}
        </div>
      
      
    </form>
  )
}

function mapStateToProps(state) {
  const { loggingIn } = state.authentication;
  return {
      loggingIn
  };
}

RegisterForm = reduxForm({
  // a unique name for the form
  form: 'login',
  validate,
  asyncValidate,
  asyncBlurFields: ['email'],
})(RegisterForm)

export default connect(mapStateToProps)(RegisterForm);