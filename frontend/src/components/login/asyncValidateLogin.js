import { authHeader } from '../../helpers/auth-header';
import { API_BASE_URI } from '../../constants/URLConstants';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));


// function composeAsyncValidators(validatorFns) {
//   return async (values, dispatch, props, field) => {
//     const validatorFn = validatorFns[field]
//     await validatorFn(values, dispatch, props, field);
//   };
// }

const usernameValidate =  (values /*, dispatch */) => {
  return sleep(1000).then(async () => { // simulate server latency
    console.log('asyncValidate username');
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };
   
    const res = await fetch(`${API_BASE_URI}user?email=${values.email}`, requestOptions).then(res => res.json())
   
    if (res && res.error && res.error ==='Not Found') {
        throw { email: 'That email does not exist' };
    }
  });
};

// const companynameValidate = (values /*, dispatch */) => {
//   return sleep(1000).then(() => { // simulate server latency
//     console.log('asyncValidate companyname')
//     if (['google','amazon','tesla'].includes(values.companyname)) {
//       throw { companyname: 'That companyname is taken' };
//     }
//   });
// };
const asyncValidate = usernameValidate;
// const asyncValidate = composeAsyncValidators({
//   email:usernameValidate
// });

export default asyncValidate;
