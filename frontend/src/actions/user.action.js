import { userConstants } from '../constants/ActionTypes';
import { userService } from '../services/user.service';
import { alertActions } from './alert.action';
import { history } from '../helpers/history';

export const userActions = {
    login,
    logout,
    getAll,
    register
};

function register(obj) {
    return dispatch => {
        dispatch(request(obj.email));

        userService.register(obj)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                    setTimeout(() => {
                        dispatch(alertActions.clear());
                    }, 5000)
                }
            );
    };

    function request(user) { return { type: userConstants.REGITSER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGITSER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGITSER_FAILURE, error } }
}

function login(obj) {
    return dispatch => {
        dispatch(request(obj.email));

        userService.login(obj)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                    setTimeout(() => {
                        dispatch(alertActions.clear());
                    }, 5000)
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function getAll() {
    return dispatch => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => { 
                    dispatch(failure(error));
                    dispatch(alertActions.error(error))
                }
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}